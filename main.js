'use strict';

import {AppRegistry} from 'react-native';
import ReactTodo from './components/todo-app';

AppRegistry.registerComponent('ReactTodo', () => ReactTodo);
